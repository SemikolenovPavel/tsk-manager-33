package ru.t1.semikolenov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.api.service.IServiceLocator;
import ru.t1.semikolenov.tm.dto.request.AbstractUserRequest;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.exception.system.AccessDeniedException;
import ru.t1.semikolenov.tm.model.User;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    @Nullable
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(AbstractUserRequest request, Role role) {
        @NotNull final User user = checkUser(request);
        if (role == null) throw new AccessDeniedException();
        @Nullable final Role userRole = user.getRole();
        if (userRole != role) throw new AccessDeniedException();
    }

    protected void check(AbstractUserRequest request) {
        checkUser(request);
    }

    @NotNull
    private User checkUser(AbstractUserRequest request) {
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getUserService().findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

}