package ru.t1.semikolenov.tm.api.client;

import ru.t1.semikolenov.tm.api.endpoint.IUserEndpoint;

public interface IUserEndpointClient extends IUserEndpoint, IEndpointClient {
}
