package ru.t1.semikolenov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.dto.request.UserLogoutRequest;
import ru.t1.semikolenov.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "logout";

    @NotNull
    public static final String DESCRIPTION = "Log out";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        serviceLocator.getAuthEndpointClient().logout(new UserLogoutRequest());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}