package ru.t1.semikolenov.tm.api.client;

import ru.t1.semikolenov.tm.api.endpoint.ISystemEndpoint;

public interface ISystemEndpointClient extends ISystemEndpoint, IEndpointClient {
}
