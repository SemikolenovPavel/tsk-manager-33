package ru.t1.semikolenov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.client.IDomainEndpointClient;
import ru.t1.semikolenov.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public IDomainEndpointClient getDomainEndpointClient() {
        return serviceLocator.getDomainEndpointClient();
    }

}