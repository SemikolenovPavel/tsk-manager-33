package ru.t1.semikolenov.tm.api.client;

import ru.t1.semikolenov.tm.api.endpoint.IProjectTaskEndpoint;

public interface IProjectTaskEndpointClient extends IProjectTaskEndpoint, IEndpointClient {
}
