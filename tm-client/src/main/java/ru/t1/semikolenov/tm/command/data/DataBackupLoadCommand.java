package ru.t1.semikolenov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.dto.request.DataBackupLoadRequest;
import ru.t1.semikolenov.tm.enumerated.Role;

public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-load";

    @NotNull
    public static final String DESCRIPTION = "Load backup from base64 file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpointClient().loadDataBackup(new DataBackupLoadRequest());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}