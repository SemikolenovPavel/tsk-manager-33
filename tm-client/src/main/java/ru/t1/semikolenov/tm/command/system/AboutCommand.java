package ru.t1.semikolenov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class AboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Show developer info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final String authorName = serviceLocator.getPropertyService().getAuthorName();
        @NotNull final String authorEmail = serviceLocator.getPropertyService().getAuthorEmail();
        System.out.println("Name: " + authorName);
        System.out.println("E-mail: " + authorEmail);
    }

}