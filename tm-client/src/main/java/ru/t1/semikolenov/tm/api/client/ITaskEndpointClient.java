package ru.t1.semikolenov.tm.api.client;

import ru.t1.semikolenov.tm.api.endpoint.ITaskEndpoint;

public interface ITaskEndpointClient extends ITaskEndpoint, IEndpointClient {
}
