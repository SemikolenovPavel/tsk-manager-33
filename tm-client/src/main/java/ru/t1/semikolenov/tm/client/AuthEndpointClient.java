package ru.t1.semikolenov.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.client.IAuthEndpointClient;
import ru.t1.semikolenov.tm.dto.request.UserLoginRequest;
import ru.t1.semikolenov.tm.dto.request.UserLogoutRequest;
import ru.t1.semikolenov.tm.dto.request.UserProfileRequest;
import ru.t1.semikolenov.tm.dto.response.UserLoginResponse;
import ru.t1.semikolenov.tm.dto.response.UserLogoutResponse;
import ru.t1.semikolenov.tm.dto.response.UserProfileResponse;

@NoArgsConstructor
public class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserProfileResponse profile(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

}