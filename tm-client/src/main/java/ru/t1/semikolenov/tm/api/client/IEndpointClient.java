package ru.t1.semikolenov.tm.api.client;

import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.Socket;

public interface IEndpointClient {

    @Nullable
    Socket connect() throws IOException;

    @Nullable
    Socket disconnect() throws IOException;

    @Nullable
    Socket getSocket();

    void setSocket(@Nullable Socket socket);

}
