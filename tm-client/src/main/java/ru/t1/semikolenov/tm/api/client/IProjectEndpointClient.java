package ru.t1.semikolenov.tm.api.client;

import ru.t1.semikolenov.tm.api.endpoint.IProjectEndpoint;

public interface IProjectEndpointClient extends IProjectEndpoint, IEndpointClient {
}
