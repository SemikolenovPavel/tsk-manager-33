package ru.t1.semikolenov.tm.api.client;

import ru.t1.semikolenov.tm.api.endpoint.IDomainEndpoint;

public interface IDomainEndpointClient extends IDomainEndpoint, IEndpointClient {
}
