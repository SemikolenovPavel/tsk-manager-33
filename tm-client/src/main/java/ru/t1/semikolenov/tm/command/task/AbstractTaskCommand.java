package ru.t1.semikolenov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.client.IProjectTaskEndpointClient;
import ru.t1.semikolenov.tm.api.client.ITaskEndpointClient;
import ru.t1.semikolenov.tm.command.AbstractCommand;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.enumerated.Status;
import ru.t1.semikolenov.tm.model.Task;
import ru.t1.semikolenov.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskEndpointClient getTaskEndpointClient() {
        return serviceLocator.getTaskEndpointClient();
    }

    @NotNull
    protected IProjectTaskEndpointClient getProjectTaskEndpointClient() {
        return serviceLocator.getProjectTaskEndpointClient();
    }

    protected void renderTasks(@NotNull final List<Task> tasks) {
        int index = 1;
        for (Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(@NotNull final Task task) {
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(task.getDateEnd()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}