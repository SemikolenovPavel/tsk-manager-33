package ru.t1.semikolenov.tm.api.client;

import ru.t1.semikolenov.tm.api.endpoint.IAuthEndpoint;

public interface IAuthEndpointClient extends IAuthEndpoint, IEndpointClient {
}

