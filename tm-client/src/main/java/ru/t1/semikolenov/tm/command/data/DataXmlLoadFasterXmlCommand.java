package ru.t1.semikolenov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.dto.request.DataXmlLoadFasterXmlRequest;
import ru.t1.semikolenov.tm.enumerated.Role;

public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        getDomainEndpointClient().loadDataXmlFasterXml(new DataXmlLoadFasterXmlRequest());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}