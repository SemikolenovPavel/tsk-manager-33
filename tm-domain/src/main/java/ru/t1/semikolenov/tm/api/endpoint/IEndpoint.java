package ru.t1.semikolenov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

public interface IEndpoint {

    @NotNull
    String SPACE = "http://endpoint.tm.semikolenov.t1.ru/";

}
