package ru.t1.semikolenov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public abstract class AbstractUserRequest extends AbstractRequest {

    @Nullable
    protected String userId;

}