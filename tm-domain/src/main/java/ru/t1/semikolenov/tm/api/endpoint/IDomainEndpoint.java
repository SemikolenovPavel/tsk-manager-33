package ru.t1.semikolenov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.dto.request.*;
import ru.t1.semikolenov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IDomainEndpoint extends IEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(SPACE, PART);
        return Service.create(url, qName).getPort(IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataBackupLoadResponse loadDataBackup(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBackupLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBackupSaveResponse saveDataBackup(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBackupSaveRequest request
    );

    @NotNull
    @WebMethod
    DataBase64LoadResponse loadDataBase64(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBase64LoadRequest request
    );

    @NotNull
    @WebMethod
    DataBase64SaveResponse saveDataBase64(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBase64SaveRequest request
    );

    @NotNull
    @WebMethod
    DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBinaryLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBinarySaveResponse saveDataBinary(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBinarySaveRequest request
    );

    @NotNull
    @WebMethod
    DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonLoadFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonSaveFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataJsonLoadJaxbResponse loadDataJsonJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonLoadJaxbRequest request
    );

    @NotNull
    @WebMethod
    DataJsonSaveJaxbResponse saveDataJsonJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonSaveJaxbRequest request
    );

    @NotNull
    @WebMethod
    DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlLoadFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlSaveFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataXmlLoadJaxbResponse loadDataXmlJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlLoadJaxbRequest request
    );

    @NotNull
    @WebMethod
    DataXmlSaveJaxbResponse saveDataXmlJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlSaveJaxbRequest request
    );

    @NotNull
    @WebMethod
    DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataYamlLoadFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull DataYamlSaveFasterXmlRequest request
    );

}
