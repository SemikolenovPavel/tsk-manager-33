package ru.t1.semikolenov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.dto.request.*;
import ru.t1.semikolenov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IProjectEndpoint extends IEndpoint {

    @NotNull
    String NAME = "ProjectEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(SPACE, PART);
        return Service.create(url, qName).getPort(IProjectEndpoint.class);
    }

    @NotNull
    @WebMethod
    ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectChangeStatusByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectChangeStatusByIndexRequest request
    );

    @NotNull
    @WebMethod
    ProjectClearResponse clearProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectClearRequest request
    );

    @NotNull
    @WebMethod
    ProjectCreateResponse createProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectCreateRequest request
    );

    @NotNull
    @WebMethod
    ProjectListResponse listProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectListRequest request
    );

    @NotNull
    @WebMethod
    ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectRemoveByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectRemoveByIndexRequest request
    );

    @NotNull
    @WebMethod
    ProjectShowByIdResponse showProjectById(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectShowByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectShowByIndexResponse showProjectByIndex(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectShowByIndexRequest request
    );

    @NotNull
    @WebMethod
    ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectUpdateByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectUpdateByIndexRequest request
    );

}
